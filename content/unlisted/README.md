---
title: Unlisted
description: hello
date: 2024-12-16
---

If you're seeing this, you probably found it looking through the source code. Nerd.

There's some stuff I want to publish, that I don't want on the main page.

Feel free to look through it but you probably won't find anything interesting. Each post should be avaliable on the web at soap.systems/unlisted/[filename.md]
