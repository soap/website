---
title: Every album I've listened to since 11/14/2023
description: Terrible music taste
---

### GUTS (spilled) - Olivia Rodrigo

**Date listened:** April 11th     
**Review:** 9/10, best pop artist of all time

![](/img/albums/guts_spilled.avif)

---

### DS2 - Future

**Date listened:** March 18th     
**Review:** 8.5/10

![](/img/albums/DS2.avif)

---

### Give Me Convenience or Give Me Death - Dead Kennedys

**Date listened:** February 28th     
**Review:** 7/10. Gets mid towards the end

![](/img/albums/give_me_convenience_or_death.avif)

---

### Everybody Can't Go - Benny the Butcher

**Date listened:** February 10th    
**Review:** 6/10. Incredibly mid with some decent songs sprinkled throughout.

![](/img/albums/everybody_cant_go.avif)

---

### american dream - 21 Savage

**Date listened:** January 6th   
**Review:** 7/10. Starts off 9/10 but gets pretty forgettable towards the middle. Also he pronounces "gif" like "jif" on "just like me"

![](/img/albums/21_savage_american_dream.jpg)

---

### Violent Femmes - Violent Femmes

**Date listened:** January 6th   
**Review:** 9/10

![](/img/albums/violent_femmes.avif)

---

### Flockaveli - Waka Flocka Flame

**Date listened:** January 5th   
**Review:** 9.5/10

![](/img/albums/flockaveli.avif)

---

### BlackBolshevik - Ghais Guevara

**Date listened:** January 2nd   
**Review:** 7.5/10

![](/img/albums/blackbolshevik.avif)

---

### Hell Hath No Fury - Clipse

**Date listened:** December 25th  
**Review:** 10/10, not a great christmas album but whatever it's awesome 

![](/img/albums/hell_hath_no_fury.avif)

---

### Quebec - Ween

**Date listened:** December 18th  
**Review:** 2/10, might be the most boring band of all time

![](/img/albums/quebec_ween.avif)

---

### Brand New Eyes - Paramore

**Date listened:** December 15th  
**Review:** 7/10, dumb teen rock at it's finest

![](/img/albums/brand_new_eyes_paramore.avif)

---

### Paramore - Paramore

**Date listened:** December 13th  
**Review:** 8/10, I came into this fully expecting to hate it but it actually turned out to be pretty good

![](/img/albums/paramore_self_titled.webp)

---

### DAMN. COLLECTORS EDITION - Kendrick Lamar

**Date listened:** December 11th  
**Review:** 10/10, pretty cool hearing it backwards

![](/img/albums/damn_collectors.avif)

---

### Playboi Carti - Playboi Carti

**Date listened:** December 8th  
**Review:** 10/10, Carti's best album

![](/img/albums/carti_self_titled.webp)

---

### Marshall Mathers LP 2 - Eminem

**Date listened:** December 8th  
**Review:** 7/10, he should've retired after this. Last decent Eminem album but you can still notice the decline

![](/img/albums/mmlp2.avif)

---

### Romantic - Mannequin Pussy

**Date listened:** December 4th  
**Review:** 8/10, cool album, listened to it after hearing their new single. Their newer albums are so lame and boring

![](/img/albums/mannequin_pussy_romantic.avif)

---

### Savage Mode II - 21 Savage

**Date listened:** December 3rd   
**Review:** 7/10, pretty cool production

![](/img/albums/savage_mode_ii.avif)

---

### uknowhatimsayin¿ - Danny Brown

**Date listened:** December 1st   
**Review:** 10/10, my favorite from him

![](/img/albums/uknowhatimsayin.avif)

---

### Bear Market - Mom Jeans.

**Date listened:** November 29th   
**Review:** 6/10, this is like a greatest hits compilation album except they re-performed each song and it's always worse than the original

![](/img/albums/bear_market.avif)

---

### fishmonger - underscores

**Date listened:** November 25th   
**Review:** 6.5/10, not entirely bad but a bit... derivative? This one didn't do much for me and it ended up being a bit boring

![](/img/albums/fishmonger.avif)

---

### 4NEM - Chief Keef

**Date listened:** November 24th   
**Review:** 7.5/10, greatest artist of all time

![](/img/albums/4nem.avif)
 
---

### Zach Bryan - Zach Bryan

**Date listened:** November 19th  
**Review:** 8/10, this might be the first modern country album that's actually good? He's got a really unique sound and I'm excited to see country finally have some real emotion out of the constant pandering from recent years

![](/img/albums/zach_bryan.avif)

---

### Modal Soul - Nujabes

**Date listened:** November 19th    
**Review:** 9/10, absolutely beautiful

![](/img/albums/modal_soul.avif)

---

### Die for My Bitch - Baby Keem

**Date listened:** November 19th  
**Favorite track:** Orange Soda  
**Review:** 7/10, huge improvement from his previous project. There's still a couple flops here but it's memorable and fun

![](/img/albums/die_for_my_bitch.avif)

---

### Hearts and Darts - Baby Keem

**Date listened:** November 18th  
**Favorite track:** Baby Keem  
**Review:** 5/10, not much to say. Not entirely bad but deeply forgettable  

![](/img/albums/hearts_and_darts.avif)

---

### Daytona - Pusha T

**Date listened:** November 17th  
**Review:** 9/10, by far Push’s best work and a quick album to listen to

![](/img/albums/daytona.avif)

---

### Wallsocket - underscores

**Date listened:** November 16th  
**Favorite tracks:** Johnny johnny johnny, Old money bitch   
**Review:** 10/10. On my first listen I gave this album a 6/10. I also couldn't stop thinking about it. Revisited it and honestly I think it's quite close to my top 10. [Also look at this tweet](https://twitter.com/HIDEO_KOJIMA_SC/status/1730268492530905103)

![](/img/albums/wallsocket.avif)

---

### Fresh Fruit for Rotting Vegetables - Dead Kennedys (2022 mix)

**Date listened:** November 16th  
**Review:** 8/10, I’m not going to pretend like I can hear any difference in the new mixing but I like the new cover more

![](/img/albums/fresh_fruit_rotting_vegetables.avif)

---

### Miseducation - Lauryn Hill

**Date listened:** November 15th  
**Review:** 10/10, I adore this album so much

![](/img/albums/miseducation.webp)

---

### Spritual Cramp - Spiritual Cramp

**Date listened:** November 14th  
**Review:** 7/10, we WILL be stealing from parties

![](/img/albums/spiritual_cramp.avif)
