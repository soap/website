---
title: Every game I've played since 11/14/2023
description: Played on console unless otherwise noted
---

### The Quarry

**Date finised:** May 12th, 2024    
**Review:** 6/10, entire first half is incredibly boring but it gets pretty fun towards the end 

---

### Nier: Automata

**Date finised:** May 7th, 2024    
**Review:** 8/10, did not like having to replay the whole game to see like 20 minutes of new content but it's fine 

---

### Like a Dragon: Infinite Wealth

**Date finised:** March 2nd, 2024    
**Review:** 9.5/10, best JRPG of all time

---

### Emily is Away
**Date finished:** January 3rd, 2024   
**Review:** 7/10, quick game but fun for what it is

---

### The Last of Us 2
**Date finished:** January 1st, 2024   
**Review:** 10/10, might be my favorite video game story

---

### Yakuza: Like a Dragon
**Date finished:** November 14th, 2023  
**Review:** 9/10, ~~best~~ second best JRPG of all time
