#!/usr/bin/env ruby

require 'sinatra'
require 'builder'
require 'time'
require 'kramdown'
require 'rouge'
require_relative 'methods'

$content_dir = 'content'
$default_title = 'the soap zone'
$default_css = '/nfo.css?v1.0'
$author = 'soap'

if settings.development?
  $domain = "http://localhost:4567"
else
  $domain = 'https://soap.systems'
end

set :public_folder, __dir__ + '/static'
set :static_cache_control, [:public, :max_age => 60 * 60 * 24 * 90]

get '/' do
  css = [$default_css]

  files = Dir.glob(File.join($content_dir, '/blog/*.md'))
  posts = get_posts(files)

  erb :index, locals: { css: css, posts: posts }
end

get '/rss.xml' do
  content_type 'text/xml'
  files = Dir.glob(File.join($content_dir, '/blog/*.md'))
  posts = get_posts(files)
  builder :rss, locals: { posts: posts } 
end

get '/*' do |filename|
  if filename.end_with? '.md' 
    file_content, front_matter = read_markdown($content_dir + '/' + filename)
    title = front_matter.fetch('title', $default_title)
    ansi_title = get_ansi_title(title)
  else
    halt(404)
  end

  html = Kramdown::Document.new(file_content, { 
    :input => 'GFM',
    :syntax_highlighter => 'rouge'
  }).to_html

  css = determine_css(html)

  erb :template, locals: { title: title, ansi_title: ansi_title, html: html, css: css }
end

not_found do
  erb :not_found, locals: { css: $default_css }
end
