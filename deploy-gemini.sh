#!/bin/sh

tar -C gemini -cvz . > gemini.tar.gz
hut pages publish gemini.tar.gz --protocol GEMINI --domain gemini.soap.systems
rm gemini.tar.gz
gemget --quiet --output /dev/null "gemini://warmedal.se/~antenna/submit?gemini%3A%2F%2Fgemini.soap.systems%2Fgemfeed.gmi"
