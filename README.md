Install dependencies with `bundle install`

Running in "production": 

`rackup -p 4567 -E production`

Development:

`rerun 'ruby app.rb -p 4567 -e development'`
