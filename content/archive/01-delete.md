---
title: Deleting your past on the internet
description: How to prevent an info leak by deleting old accounts across the internet
date: September 12th, 2023
---

<p><small><em>need help? <a href="../../contact.html">contact me.</a></em></small></p>

## deleting vs disappearing

a lot of people don't realize this, but no matter how many tweets you delete, or how many accounts you try to scrub from the internet, *someone* has them saved. the only thing you can do is politely request deletion and make it harder to find.

## tools 

* [holehe](https://github.com/megadose/holehe) - email addresses
* [sherlock](https://github.com/sherlock-project/sherlock) - usernames

## using holehe

installing holehe is easy enough, just run `pip install holehe`

run `holehe --only-used email@address.com` to get a non-exhaustive list of websites you've signed up to with that email, and repeat for every email address you've ever used.

pick the ones you want to delete and save them in a list, but don't start deleting just yet.

## using sherlock

install sherlock with `git clone https://github.com/sherlock-project/sherlock && cd sherlock && pip install -r requirements.txt` 

sherlock is only for usernames, you should search for every username you can remember. a good trick is to search for your email username (if your email address is soap1234@gmail.com, your email username is soap1234). accounts that generate info based off of your email username use your real name a lot.

save the accounts you want deleted and proceed to EPIEOS

## using EPIEOS

[EPIEOS](https://epieos.com) is pretty useful, but it doesn't show you a lot of info without a login (which you should not make). it can, however, show your google calender, all of the google reviews you've left, along with some accounts that the other tools can't get. i personally had a chess.com account with my real name on it that holehe and sherlock didn't pick up. 

## deleting your accounts

this is the most important step. before you start deleting accounts, you should check [web.archive.org](https://web.archive.org) for copies of that profile. if there aren't any copies, you're good to delete the account and move on to the next. if there *are* copies, you should change the bio to something like "This is a formal request to remove this account, and all related pages, from the Internet Archive and Wayback Machine." Then, you can send an email to info@archive.org, with the subject "Social media account removal" that says "I am [username] on [website.com]. I am officially requesting the removal of [URL] from the Wayback Machine, along with all pages that begin with [website.com/username]. There is verification at [website.com/profile]." from there, you should wait until you get a reply or the page gets removed from the archive, at which point you can delete the account using instructions from [justdeleteme.xyz](https://justdeleteme.xyz).