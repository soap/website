require 'yaml'
require 'ruby_figlet'
using RubyFiglet

def read_markdown(filename)
  front_matter_regex = /\A---\s*\n(.*?)\n?^---\s*$\n?/m
  file_content = File.read(filename)
  front_matter = get_front_matter(filename)
  markdown = file_content.sub(front_matter_regex, '')
  [markdown, front_matter]
end

def get_front_matter(filename)
  file_content = File.read(filename)
  front_matter = YAML.load(
    file_content,
    permitted_classes: [Date] # this actually breaks the script on at least ruby 2.5.1 
  )
end

def determine_css(html)
  css = [$default_css]
  css << '/highlight.css' if html.include? '<div class="highlight">'
  css
end

def get_posts(files)
  posts = []

  files.each do |file_path|
    front_matter = get_front_matter(file_path)
    posts << { 
      title: front_matter['title'], 
      date: front_matter['date'],
      link: $domain + file_path.sub($content_dir, ''), # sub $content_dir here because it isn't in the url
      description: front_matter['description']
    } 
  end

  posts.sort_by! { |entry| entry[:date] }.reverse!
  posts
end

def get_ansi_title(title)
  words = title.split
  words = title.split.map do |word|
    word = word.concat(' ')
    "<div class=\"ansi-word\"><pre>#{word.art('ANSI Shadow')}</pre></div>"
  end
  words.join(' ')
end
