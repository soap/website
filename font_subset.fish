#!/usr/bin/env fish

if ! which fonttools >/dev/null
    pip install fonttools zopfli brotli
end

if ! which glyphhanger >/dev/null
    npm install -g glyphhanger
end

# get a list of every character seperated by newlines, remove duplicates, remove newlines, then remove tabs
# I love shell scripting but goddamn it's an unreadable mess
set -l used_characters (grep -hro . content | sort -u | tr -d '\n' | tr -d '\t')

for file in static/fonts/*subset*
    rm $file
end

for file in static/fonts/*.woff2 static/fonts/*.ttf
    # spits out a bunch of warnings, just some weird metadata thing
    glyphhanger --whitelist=$used_characters --subset=$file --formats=woff2
end
