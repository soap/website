---
title: Donations
---

I only accept donations in crypto beacuse I want to stay anonymous. I'm not broke or anything though so only give me money if you can afford it.

Solana: soapX9gJz9hd2fow5yNGj2nVaCAZdCoCHL1DBNjhL8G, [QR code](/img/donate/solana-qr.avif)

Monero: [49soap2DhWnbTN4yyDux3dEE2jMVNohi6PR5vzE71Zg44mYQaJhKq6W9qLRMj4RbBYCPPzP55J1ZB6uVepKu5sTn1nwy3bQ](monero:49soap2DhWnbTN4yyDux3dEE2jMVNohi6PR5vzE71Zg44mYQaJhKq6W9qLRMj4RbBYCPPzP55J1ZB6uVepKu5sTn1nwy3bQ), [QR code](/img/donate/monero-qr.avif)

Ethereum: [0x5049bcdcc099F2738347512575c7Be39e8E8F53f](ethereum:0x5049bcdcc099F2738347512575c7Be39e8E8F53f), [QR code](/img/donate/eth-qr.avif)

Nano: [nano_1soap1b39gyjtypnfoj8emt6f8nqxajq3xomfghwewgntegnodtbd9jgaacu](xrb:nano_1soap1b39gyjtypnfoj8emt6f8nqxajq3xomfghwewgntegnodtbd9jgaacu), [QR code](/img/donate/nano-qr.avif)

Zcash: [u12tzy9k5upmnt4p4pcejlg78xm9zywwwgy6j6e4jw4prmyfud4jg65j9psel9kr06336t08zu7aull8qm836lrx8e9mlp5t492s5d6k34s96dsvm9qs8pqja7czy90mjrxpmkgegjwv8z4arajyaftn968644a04nxx9alxtc8s64hjfawrkdr4njwg7mfd3cu2z9rhdc0xu3jkensnn](zcash:u12tzy9k5upmnt4p4pcejlg78xm9zywwwgy6j6e4jw4prmyfud4jg65j9psel9kr06336t08zu7aull8qm836lrx8e9mlp5t492s5d6k34s96dsvm9qs8pqja7czy90mjrxpmkgegjwv8z4arajyaftn968644a04nxx9alxtc8s64hjfawrkdr4njwg7mfd3cu2z9rhdc0xu3jkensnn), [QR code](/img/donate/zcash-qr.avif)

Bitcoin: [1SoapnzrLFf227tWyZVL9TS99pYiVzowF](bitcoin:1Soapnzrlff227twyzvl9ts99pyivzowf), [QR code](/img/donate/btc-qr.avif)
