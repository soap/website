---
title: Antisearch
header-includes: >
    <meta name="robots" content="noindex, nofollow" />
    <meta name="googlebot" content="noindex">
    <meta name="bingbot" content="noindex">
    <meta name="baiduspider" content="noindex">
---

This is my [antisearch](https://retr0.id/stuff/antisearch/about.html) page. I try to keep links here that are hard to find because of SEO or de-indexing

## Cool or otherwise unsorted websites

[Anna's blog](https://annas-blog.org/) - blog from the owners of annas-archive, the successor to zlib and libgen

[sizeof.cat](https://sizeof.cat) - cool enough that verizon blocks it

[melonking.net](https://melonking.net)

[after the beep - publicly accessible voicemail box](https://afterthebeep.tel)

## Old websites

[Internet Explorer is evil](http://toastytech.com/evil/index.html)

[mozilla kicks ass](http://toastytech.com/good/index.html)

[heavensgate.com](https://www.heavensgate.com/) - i wonder who's hosting this? interesting to look through

## Experiences/games

[ozwomp.online](https://ozwomp.online/) - fly around as little space guy. one of my favorite websites of all time

## Guides

[Tech independence](https://sive.rs/ti) - self hosting cloud storage, contact, calender and email

[Run your own social](https://runyourown.social/) - guide for setting up a small fedi instance for your friends

## Videos

[Lupe Fiasco presents "Rap Theory & Practice: an Introduction"](https://youtu.be/zBHRsYhYb-o)

[Professor Skye's Record Review](https://youtube.com/@professorskye)

[The Right-Wing War on Education](https://youtu.be/bo0M3H1QsEQ)

[Dirty Girls](https://youtube.com/watch?v=h3MxEHQk644)

## Nerd stuff

[The cursed computer iceberg](https://suricrasia.online/iceberg/) - one of those iceberg memes but it actually has context this time 

## Science

[transfemscience.org](https://transfemscience.org)

## Web 1.0

[How to save a complete copy of a website](https://wiki.archiveteam.org/index.php/Wget#Creating_WARC_with_wget)

[gifcities](https://gifcities.org/) - gifs from geocities websites

[Web badges world](https://web.badges.world/)

[88x31 gifs](https://cyber.dabamos.de/88x31/) - more badges

[Capstasher badges](https://capstasher.neocities.org/88x31collection-page1) - even more badges, claims to be the largest archive

[Yesterweb badges](https://yesterweb.org/graphics/buttons) - modern badges

[Neonaut's badges](https://neonaut.neocities.org/cyber/88x31) - so many badges

[Netscape badges](http://www.textfiles.com/underconstruction/netscape/)

[Cameron's world](https://www.cameronsworld.net/) - old geocities pages, some of the text and images have links

[wiby](https://wiby.me) - search engine for the small web

[status.cafe](https://status.cafe) - microblogging on one big timeline

## Internet history

[archiveteam](https://wiki.archiveteam.org/) - archives most popular websites

[archiveteam warrior](https://wiki.archiveteam.org/index.php/ArchiveTeam_Warrior) - croudsourced archiving, like folding@home

[oldavista](https://oldavista.com) - old websites from the internet archive and theoldnet.com</p>

[textfiles.com](https://textfiles.com) - text files from early BBSs

## Thoughts

Blog posts that are insightful enough to get their own spot here

[what is the web revival?](https://thoughts.melonking.net/guides/introduction-to-the-web-revival-1-what-is-the-web-revival)

## Tools

[like-th.at](https://like-th.at/) - domain hack generator

## News

[tildes.net](https://tildes.net) - aggregator, like a cross between hacker news and old reddit, currently in invite-only alpha, but you can still read posts (or contact me if you want an invite)

[lobsters](https://lobste.rs/) - similar in style to tildes.net, with a stronger focus on tech

## Smallweb

[tildeverse.org](https://tildeverse.org)

[kagi smallweb](https://kagi.com/smallweb) - random blog post from a smallweb site

[tildegit](https://tildegit.org/) - gitea instance for the tildeverse. needs an email address from one of the [approved domains](https://tildegit.org/assets/allowed_domains.txt) to sign up

[tilde.wiki](https://tilde.wiki) - list of tildeverses and some history behind them

## Silly novelty websites

[spam.budwin.net](https://spam.budwin.net/) - guide to spam-like products. click on the pictures.

[hhhhhhhhhhh.website](http://hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh.website/)

[meow.camera](https://meow.camera)

[StreetCatWiki](https://streetcat.wiki/index.php/Main_Page)

[matias.me/nsfw](https://matias.me/nsfw/)

## Etc

[Random wikipedia page](https://en.wikipedia.org/wiki/Special:RandomPage)

## Similar pages

[tris.fyi bookmarks](https://tris.fyi/bookmarks.html)

# Gemini

[My Site](https://bridge.soap.systems/gemini://gemini.soap.systems)

[My Gemini > HTTP Bridge](https://bridge.soap.systems/)

## Introductions

[Gemini Quickstart](https://bridge.soap.systems/gemini://geminiquickst.art)

## Similar pages

[CAPCOM Aggregator](https://bridge.soap.systems/gemini://gemini.circumlunar.space/capcom/)

[Antenna](https://bridge.soap.systems/gemini://warmedal.se/~antenna/)

## Tools

[Gemini Client Torture Test](https://bridge.soap.systems/gemini://gemini.thebackupbox.net/test/torture)

<details>
  <summary>memetic apiopage</summary>
  <a href="https://citrons.xyz/a/memetic-apioform-page.html"><img src="https://citrons.xyz/a/static/apiopage.png"/></a>
</details>

<details>
  <summary>johnvertisement</summary>
  <iframe src="https://john.citrons.xyz/embed?ref=soap.systems/antisearch.md" style="margin-left:auto;display:block;margin-right:auto;max-width:732px;width:100%;min-width:600px;height:94px;border:none;" loading="lazy"></iframe>
</details>

<details>
  <summary>neolink</summary>
  <div style="display: flex; gap: 10px;"> 
   <iframe width="180" height="180" style="border:none" src="https://googol.neocities.org/neolink/embed.html" name="neolink" loading="lazy"></iframe>
   <iframe width="180" height="180" style="border:none" src="https://dimden.neocities.org/navlink/" name="neolink" loading="lazy"></iframe>
  </div>
  <iframe width="468" height="60" style="border:none" src="https://hbaguette.neocities.org/bannerlink/embed.html" name="bannerlink" loading="lazy"></iframe>
</details>

<details>
<summary>run a snowflake proxy in your browser</summary>
<div id="snowflake">
<ol>
 <li>turn on the switch next to "enabled" below to turn snowflake on and become a snowflake proxy volunteer.</li>
 <li>keep this page open to continue to be a snowflake proxy volunteer. close it to stop being one. come back to start being one again.</li>
 <li>if it says "cookies are not enabled.", open <a target="_blank" href="https://snowflake.torproject.org/embed.html">https://snowflake.torproject.org/embed.html</a> and follow the same instructions as above but on said page.</li>
</ol>
<iframe src="https://snowflake.torproject.org/embed.html" width="320" height="240" frameborder="0" scrolling="no"></iframe>
</div>
</details>
