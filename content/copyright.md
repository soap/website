---
title: Copyright
---

All content on this website is licensed under [CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en), unless explicity noted. This essentially means that you must give credit, use one of [these licenses](https://creativecommons.org/share-your-work/licensing-considerations/compatible-licenses/) and you must not use material for commercial purposes.

The site generator is my own creation and it's licensed under AGPLv3.
