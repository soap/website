---
title: Extracting assets from Pepsiman (and most other PSX games) 
description: I really like Pepsiman
date: 2024-04-18
---

Pepsiman is a really funny game, and I wanted the assets for it. This isn't too hard to do but old forum posts are full of annoying people and I don't really want to dig through the info again, so I'm documenting it here.

First, you need to download a copy of Pepsiman along with some software. I got mine from [Myrient](https://myrient.erista.me), but if you're a nerd and you want to rip your own copy that's probably fine. BIN rips preferred over ISO.

 To extract the textures and models I care about, I used [PSXPrev](https://github.com/rickomax/psxprev) and for the movie files, I used [jPSXdec](https://github.com/m35/jpsxdec) (it isn't directly packaged for linux but you can just run the .jar with OpenJDK). ffmpeg can actually directly convert the .STR files that the PS1 used, but I find that its color and quality is worse than uncompressed AVI with jPSXdec. Unfortunately, uncompressed AVI is obscenely large, taking up 368MB for 2 minutes of 320x240 video and compressed AVI is laughably bad. I settled on exporting to uncompressed AVI, then converting to MP4 with ffmpeg. The converted video on the default settings wasn't playing, so you need to add `-pix_fmt yuv420p` to the ffmpeg command. No clue why, but I saw the answer on Stack Overflow and it works fine.

```shell
# Convert every .avi file into a useable MP4
# fish shell only
for file in *.avi
  ffmpeg -i $file -pix_fmt yuv420p (path change-extension MP4 $file)
end
```

Here's the final videos: 

<video src="/img/pepsiman/MOVIE0.MP4" controls></video>
<video src="/img/pepsiman/MOVIE1.MP4" controls></video>
<video src="/img/pepsiman/MOVIE2.MP4" controls></video>
<video src="/img/pepsiman/MOVIE3.MP4" controls></video>
<video src="/img/pepsiman/MOVIE4.MP4" controls></video>
<video src="/img/pepsiman/MOVIE5.MP4" controls></video>

I tried to upscale the videos using video2x, but it took a long time with disappointing results, almost like the video had been smeared with vaseline. Let me know if you're a video nerd and you can upscale/denoise these videos.

Extracting textures is much easier. Just select your track 1 .bin file in PSXPrev and sort through the textures and models you want.
