# The Beautiful Simplicity of Ice Spice

People who dislike artists like Ice Spice don't fully understand the power of communal music. You can't get a club full of people to dance along to MF DOOM or your favorite goreshit producer with 12 soundcloud followers because they just can't excite everyone in the same way. 


Complexity is not the most important measure of quality and it will never be. Ice Spice is a charismatic artist with funny lyrics and that is all she needs to be good. The entire point of her music is stupid fun and it's good to embrace that. You simply can't tell me that "Think U The Shit (Fart)" isn't a hilarious title, it's okay for artists to be immature! I think we've circled back to mumble rap discourse.

> “I wouldn't consider myself a lyricist. [...] I want them to be digestible, I don't want them to fly over people's heads and they never catch it. I want people to hear it right away and be like, ‘OK, that was cute.’ But it's also fun at the same time.”
> ― Ice Spice
