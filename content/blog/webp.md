---
title: My thoughts on WebP
description: JXL my beloved
date: 2023-11-14
---

I really don't understand the hate for WebP. It was sort of sabotaged by Adobe (they didn't support it for 13 years!) but I don't think that stops it from being a great format. Pretty much every image on this site is WebP which helps a lot for load times on image-heavy pages like the album one, but no one even notices it because it functions fine until you try to import it into your shitty pirated paint app from 2009. There's really no difference in quality, even with a png 10x it's size.

WebP also works on [every modern browser](https://caniuse.com/webp) past 2014 and it completely replaces gif, jpg and png with it's lossless/lossy compression, animation and transparency. I wish it came earlier, but I find that everything I use supports WebP by now.

Here's the bash function I use to convert images to WebP. Just add it to your .bashrc and run`optimize filename`and it'll replace your old images with WebP. You can also specify quality with the -q flag.

```bash
optimize() {
    local quality=80
    while [ "$#" -gt 0 ]; do
        case "$1" in
            -q)
                quality="$2"
                shift 2
                ;;
            *)
                break
                ;;
        esac
    done
    echo "Quality: $quality"
    for file in "$@"; do
        if [[ $file != *.webp ]]; then
            basename=${file%.*}
            cwebp -m 6 -q $quality "$file" -o "$basename.webp"
            rm "$file"
        else
            mv "$file" "$file.old"
            cwebp -m 6 -q $quality "$file.old" -o "$file"
            rm "$file.old"
        fi
    done
}
```

For a comparison, here's an image I was using on the albums page (RIP). WebP gets it 10x smaller with no noticeable difference.

<figure>
<img src="/img/miseducation_uncompressed.jpg" alt="The Miseducation of Lauryn Hill album cover, repeated twice, with little to no visual difference">
<figcaption>JPG, 3.8M</figcaption> 
</figure>

______________________________________________________________________


<figure>
<img src="/img/miseducation.webp">
<figcaption>WebP, 380K</figcaption> 
</figure>

______________________________________________________________________

As a bonus, here's the same image, dithered and converted to WebP (on lossless mode, lossy actually makes the image bigger).

______________________________________________________________________

<figure>
<img src="/img/miseducation_dithered.webp">
<figcaption>20K</figcaption> 
</figure>

______________________________________________________________________

Dithering is a pretty great way to reduce file size by a lot if the content is largely aesthetic, but obviously fails if you need images with any detail. [Low Tech Magazine](https://solar.lowtechmagazine.com/) has entirely dithered images for a pretty cool effect. 

