---
title: Contact
header-includes: >
    <meta name="robots" content="noindex" />
---

*  Bluesky - [soap.systems](https://bsky.app/profile/did:plc:mbdv5fpty2wlssfwwpys2boh)
*  Twitter - [@s0ap_nation](https://twitter.com/i/user/1123982013307019265)
*  Email - [soaper@disroot.org](mailto:soaper@disroot.org)

I'm also looking for a riseup.net invite, if you have one

# Rarely used accounts

These socials aren't checked very often

*  Cohost - [@soap-](https://cohost.org/soap-)
*  Twitter (alt) - [@soap_salt](https://twitter.com/i/user/1675953102254493699)
*  Discord - [soap.systems](https://discord.com/users/589650351297396738)
*  Tumblr - [soap-nation](https://soap-nation.tumblr.com)
