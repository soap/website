---
title: Smart TVs kinda suck
description: I hate you Amazon
date: 2023-11-07
---

Why is it so hard to find a smart TV with good UX? LG's WebOS is a frustrating mess, Samsung's Tizen isn't android-based, making it pretty much useless for smart TV stuff, Amazon's FireOS is so inexplicably slow that it's unusable, Roku is a [walled garden](<https://en.wikipedia.org/wiki/Walled_garden_(technology)>) that shoves ads in your face on a TV that you paid full price for. All of them except Roku don't even have a place where you can combine all your streaming services into one browsing experience. Even my PlayStation has that.

<figure>
<img src="/img/lg-webos.webp" alt="TV running WebOS. There's some kid holding balloons taking up most of the screen space and all the apps are strange rhombus-like slices">
<figcaption>Seriously, who designed this? Whose kid is that? What does
that woman’s cat have to do with the HDMI input?</figcaption>
</figure>

For all of these TVs (which may have very well been over $2000), users come second. As with everything "smart", it's real purpose is to collect and sell data. According to their privacy policy, Samsung collects your name, address, phone number, date of birth, demographic info, your username on third-party services, photos, contacts, text logs, calendar information, voice recordings, and information about products you purchase. Hell, recently a ["free TV"](https://wired.com/story/telly-tv-free-privacy/) company popped up (as long as you keep the camera on at all times).

There's far too many devices that can perfectly replicate the smart TV experience without all the infuriating parts. Get an Apple TV, PlayStation or just cast your phone. I promise it's fine.
