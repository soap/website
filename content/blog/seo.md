---
title: Why SEO ruined the internet
description: Honestly 99% of tech jobs are fake
date: 2024-01-08
---

Imagine, it's 1996, you're new to the World Wide Web and you want to find information about your relatively obscure hobby, soap carving. How would you actually go about this? The only way to find new webpages is by visiting other people's webpages and hoping you find a link that might be a little bit related to soap carving.

Okay, now imagine it's 1998. You still haven't found any soap carving websites when you hear about this new website called Google. Apparently, it lets you search the whole web for whatever you want and suddenly there's a near infinite amount of soap carving knowledge at your disposal.

Fast forward around 10 years and you've used all the knowledge you gained from the web to open an online soap business, all from your home. But, for some reason, no one can find your website. You may have 10 years of soap making experience, but all of your potential customers only click the first result for "buy soap online". Apparently the online soap business has a lot of competition, so how do you get to the top? You have to implement all sorts of measures that make your website appear better to the search engine, but end up making the UX worse for real people.

So, you start pumping out articles about soap, soap carving, soap molds, soap accessories, soap dispensers, shampoo, dish soap, medicated soap and liquid soap, even though you don't know anything about most of these topics. The search engine doesn't know you're lying and your website shoots to the top of the results because you stuffed it full of keywords that made Google think your website was a great resource about soap. Eventually, everyone else catches on and starts writing garbage about soap products too. Slowly, all of the useful soap content written by professionals that you learned from gets relegated to the 6th page of results, and will probably never see any visitors from a search engine.

The future of search engines is pretty bleak. The content is already bad enough and I think soon enough there's going to be completely automated AI content farms. As a quick demo, I asked the [LLaVA 1.5 model](https://github.com/Mozilla-Ocho/llamafile) to "write an SEO optimized article about soap" and [here's](seo_hell.txt) what I got. This didn't cost anything and took about 5 minutes to generate on my laptop's integrated graphics.
