---
title: The Beautiful Simplicity of Ice Spice
description: (Fart)
date: 2024-02-29
---

People who dislike artists like Ice Spice don't fully understand the power of communal music. You can't get a club full of people to dance along to MF DOOM or your favorite goreshit producer with 12 soundcloud followers because they just can't excite everyone in the same way. 


Complexity is not the most important measure of quality and it will never be. Ice Spice is a charismatic artist with funny lyrics and that is all she needs to be good. The entire point of her music is stupid fun and it's good to embrace that. You simply can't tell me that "Think U The Shit (Fart)" isn't a hilarious title, it's okay for artists to be immature! I think we've circled back to mumble rap discourse.

> “I wouldn't consider myself a lyricist. [...] I want them to be digestible, I don't want them to fly over people's heads and they never catch it. I want people to hear it right away and be like, ‘OK, that was cute.’ But it's also fun at the same time.”
> &mdash; Ice Spice
<!-- https://www.complex.com/music/a/backwoodsaltar/ice-spice-lyricist-bars-digestible -->

## Addendum 2024-12-10

The time of year has me thinking about this topic again. Rap is the only genre of music, maybe aside from pop (except in recent years), that gets this level of scrutiny for "meaningless" lyrics. Christmas music is the most repetitive genre, we've been hearing the same songs with the same lyrics for 50+ years now, but it doesn't get anyway near the same scrutiny as Ice Spice or Sexyy Red or any of the "mumble rappers. 

Even in hip hop communities, female rappers get more hate than male rappers for talking about the same things. It's completely normal to rap about fucking women but once women start rapping about being fucked it's the worst thing to ever happen to rap.
