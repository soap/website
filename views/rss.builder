xml.instruct! :xml, :version => '1.0'
xml.rss :version => "2.0" do
  xml.channel do
    xml.title $default_title
    xml.link $domain
    xml.description "Recent content at #{$domain.sub('https://', '')}"
    xml.language "en-us"
    xml.managingEditor "soaper@disroot.org (soap)"

    posts.each do |post|
      xml.item do
        xml.title post[:title]
        xml.link post[:link]
        xml.description post[:description]
        xml.pubDate post[:date].rfc822 
      end
    end
  end
end
